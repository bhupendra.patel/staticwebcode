package com.amdocs;

import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testAdd() throws Exception {

        final int calculator = new Calculator().add();
        assertEquals("Add", 9, calculator);
        
    }

    @Test
    public void testSub() throws Exception {

        final int calculator= new Calculator().sub();
        assertEquals("Sub", 3, calculator);

    }
}

