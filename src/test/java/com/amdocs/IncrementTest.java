package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {
    @Test
    public void testDecreaseCounter() throws Exception {

        final int increment = new Increment().decreasecounter(10);
        assertEquals("Decrease Counter", 2, increment);
        
    }

    @Test
    public void testDecreaseCounterOther() throws Exception {

        final int decrement= new Increment().decreasecounter(0);
        assertEquals("Decrease Counter", 0, decrement);
        
    }    
}

